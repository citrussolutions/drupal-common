# Install gulp workflow packages after composer stuff is installed
npm install --prefix ./vendor/citrussolutions/gulp-workflow

ln -s ./vendor/citrussolutions/gulp-workflow/gulpfile.babel.js
ln -s ./vendor/citrussolutions/gulp-workflow/node_modules/
ln -s "$PWD"/gulp-config.json vendor/citrussolutions/gulp-workflow/
