#!/bin/bash
#
# Lando based Drupal theme automation script.
#
# Author:: Mika Lahtinen <mika.lahtinen@citrus.fi>
# 14.12.2018

# clear && cat "$LANDO_MOUNT/scripts/citrus/epic-ascii-gfx"


# Ask for theme name
echo
read -e -p "Enter theme name [$LANDO_APP_NAME]: " THEME_NAME
THEME_NAME=${THEME_NAME:-$LANDO_APP_NAME}

# Clone template theme
composer require --dev citrussolutions/citrus-d8-theme-template

THEME_PATH="$LANDO_WEBROOT/themes/custom/$THEME_NAME"
THEME_TEMPLATE_PATH="$LANDO_MOUNT/vendor/citrussolutions/citrus-d8-theme-template"

# Copy as new theme
chmod 755 "$LANDO_WEBROOT/sites/default"
mkdir -p $THEME_PATH
cp -r $THEME_TEMPLATE_PATH/* $THEME_PATH

# Rename files
mv $THEME_PATH/THEME_NAME.breakpoints.yml $THEME_PATH/$THEME_NAME.breakpoints.yml
mv $THEME_PATH/THEME_NAME.info.yml $THEME_PATH/$THEME_NAME.info.yml
mv $THEME_PATH/THEME_NAME.libraries.yml $THEME_PATH/$THEME_NAME.libraries.yml
mv $THEME_PATH/THEME_NAME.theme $THEME_PATH/$THEME_NAME.theme

# Make some replacements
sed -i 's/{{THEME_NAME}}/'$THEME_NAME'/g' $THEME_PATH/$THEME_NAME.breakpoints.yml
sed -i 's/{{THEME_NAME}}/'$THEME_NAME'/g' $THEME_PATH/$THEME_NAME.info.yml
sed -i 's/{{THEME_NAME}}/'$THEME_NAME'/g' $THEME_PATH/$THEME_NAME.theme

# Copy maintenance page templates for new theme
cp $LANDO_WEBROOT/core/modules/system/templates/maintenance-*.twig $THEME_PATH/templates

# # Create gulp-config.json
# touch "$LANDO_MOUNT/gulp-config.json"
# echo "{\"theme_root\" : \"web/themes/custom/$THEME_NAME/\" }" >> "$LANDO_MOUNT/gulp-config.json"
# echo

echo "Now we should run gulp related install'z"
