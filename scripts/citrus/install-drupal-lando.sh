#!/bin/bash
#
# Lando based Drupal 8 install script.
#
# Author:: Mika Lahtinen <mika.lahtinen@citrus.fi>
# 13.12.2018

DRUPAL_INSTALL_LOCALE="fi"
DRUPAL_ADMIN_USERNAME="citrus-admin"
DRUPAL_ADMIN_PASSWORD=`openssl rand -hex 12`
DRUPAL_SITE_EMAIL="drupal@citrus.fi"
SETTINGS_FILE_PATH="$LANDO_MOUNT/scripts/citrus/settings"

# Read site name
read -e -p "Enter Drupal 9 site name [$LANDO_APP_NAME]: " SITE_NAME
SITE_NAME=${SITE_NAME:-$LANDO_APP_NAME}

# Install composer packages
cd $LANDO_MOUNT
composer clear-cache
composer install

# Misc stuff
chmod 755 "$LANDO_WEBROOT/sites"
chmod 755 "$LANDO_WEBROOT/sites/default"
chmod 644 "$LANDO_WEBROOT/sites/default/settings.php"

# Copy settings
cp "$SETTINGS_FILE_PATH/settings.php" "$LANDO_WEBROOT/sites/default/"
cp "$SETTINGS_FILE_PATH/settings.lando.php" "$LANDO_WEBROOT/sites/default/"
cp "$SETTINGS_FILE_PATH/settings.local.php" "$LANDO_WEBROOT/sites/default/"
cp "$SETTINGS_FILE_PATH/settings.platformsh.php" "$LANDO_WEBROOT/sites/default/"
cp "$SETTINGS_FILE_PATH/development.services.yml" "$LANDO_WEBROOT/sites/"

# Install Drupal
echo "Installing $SITE_NAME Drupal..."
drush si -y minimal \
  --root=$LANDO_WEBROOT \
  --locale="$DRUPAL_INSTALL_LOCALE" \
  --db-url=mysql://drupal9:drupal9@database:3306/drupal9 \
  --db-su=drupal9 \
  --db-su-pw=drupal9 \
  --site-name="$SITE_NAME" \
  --account-name="$DRUPAL_ADMIN_USERNAME" \
  --account-pass=$DRUPAL_ADMIN_PASSWORD \
  --account-mail=$DRUPAL_SITE_EMAIL \
  --site-mail=$DRUPAL_SITE_EMAIL \

drush config-set "system.site" uuid null -y
# Not sure if this a good way to do this but at least it seems to work and
# --existing-configs didn't.

# We need to do this multiple times because configs depend on configs.
drush config-import -y
drush config-import -y
drush config-import -y

# Uninstall Update Manager module.
drush pmu update -y

drush pmu citrus_site_install -y
drush en citrus_site_install -y

# Set language to finnish and update translations.
drush cset system.site default_langcode fi -y
drush locale-check
drush locale-update
drush cr

cat <<EOF

-< $SITE_NAME done! >------------------------------------

Drupal: http://$LANDO_APP_NAME.lndo.site/user/login

User:      $DRUPAL_ADMIN_USERNAME
Password:  $DRUPAL_ADMIN_PASSWORD

EOF
