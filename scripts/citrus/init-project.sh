#!/bin/bash
#
# Lando based local development automation script.
#
# Author:: Mika Lahtinen <mika.lahtinen@citrus.fi>
# 19.12.2018

clear && cat "./scripts/citrus/epic-ascii-gfx"

ASK_FOR_THEME=false

# Require project name
if [ "$#" -eq 1 ]; then
    DEFAULT_NAME="$1"
  else
    DEFAULT_NAME="${PWD##*/}"
fi

read -e -p "Enter project name [$DEFAULT_NAME]: " PROJECT_NAME
PROJECT_NAME=${PROJECT_NAME:-$DEFAULT_NAME}
PROJECT_NAME=${PROJECT_NAME//[^a-zA-Z0-9]/}
PROJECT_NAME="$(tr [A-Z] [a-z] <<< "$PROJECT_NAME")"

echo
echo "Using project name: $PROJECT_NAME"
echo

sed -i '' -e 's/{{PROJECT_NAME}}/'$PROJECT_NAME'/g' ./.lando.yml
sed -i '' -e 's/{{PROJECT_NAME}}/'$PROJECT_NAME'/g' ./.platform.app.yaml

# Start up
lando start

# Drupal 8
while true; do
  read -p  "Environment done, install Drupal Common? (y/n) " INSTALL_DRUPAL
  case $INSTALL_DRUPAL in
    [Yy]* ) 
      lando install-drupal
      ASK_FOR_THEME=true
      break;;
    * )
      break;;
  esac
done;

# Theme
if [ "$ASK_FOR_THEME" = true ] ; then
  while true; do
    read -p  "Create theme? (y/n) " CREATE_THEME
    case $CREATE_THEME in
      [Yy]* ) 
        lando create-drupal-theme
        break;;
      * )
        break;;
    esac
  done;
fi