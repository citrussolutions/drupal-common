<?php

namespace Drupal\citrus_adminbar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request; // Is this needed?
use Drupal\Core\Url;

/**
 * Defines a route controller Clearing caches controller.
 */
class ClearCacheController extends ControllerBase {

  public function ClearCache() {
      drupal_flush_all_caches();
      \Drupal::messenger()->addStatus(t('All caches cleared.'));
      return new \Symfony\Component\HttpFoundation\RedirectResponse(\Drupal::url('<front>'));
  }
}
