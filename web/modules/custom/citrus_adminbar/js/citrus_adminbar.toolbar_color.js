(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.behaviors.adminToolbarColor = {
    attach: function () {
      if (!!$('#toolbar-bar')) {
        $('#toolbar-bar > h2').css({"color":"white","float":"left","margin":"0.6em 0.6em 0","font-size":"1.4em"});
        var base_url = window.location.origin;
        if (~base_url.indexOf("kittila.fi") || ~base_url.indexOf("master-7rqtwti-lrtk5tybs67nm")) {
          $('#toolbar-bar > h2').html('KittiläFI-TUOTANTO');
          $('#toolbar-bar').css({"background": "#990000"});
        }
        else if (~base_url.indexOf("lndo")) {
          $('#toolbar-bar > h2').html('KittiläFI-LANDO');
          $('#toolbar-bar').css({"background": "#000066"});
        }
        else if (~base_url.indexOf("develop-sr3snxi-lrtk5tybs67nm")) {
          $('#toolbar-bar > h2').html('KittiläFI-DEVELOP');
          $('#toolbar-bar').css({"background": "#006600"});
        }
        else if (~base_url.indexOf("platformsh")) {
          $('#toolbar-bar > h2').html('KittiläFI - MUU TESTI');
          $('#toolbar-bar').css({"background": "#006600"});
        }
        $('#toolbar-bar > h2').removeClass('visually-hidden');
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
