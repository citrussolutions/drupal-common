<?php

namespace Drupal\citrus_banner\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'BannerTextBlock' Block.
 *
 * @Block(
 *   id = "banner_text_block",
 *   admin_label = @Translation("Crisis banner block"),
 *   category = @Translation("Crisis banner block"),
 * )
 */
class BannerTextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $banner_content= [];
    $display = \Drupal::config('citrus_banner.settings')->get('display_banner');
    $banner_content['display'] = $display;
    $text = \Drupal::config('citrus_banner.settings')->get('banner_text');
    $text_value = check_markup($text['value'], 'basic_html');
    $banner_content['text'] = $text_value;

    $link = \Drupal::config('citrus_banner.settings')->get('link_url');
    $banner_content['url'] = $link;

    $level = \Drupal::config('citrus_banner.settings')->get('crisis_level');
    $banner_content['level'] = $level;

    $alert_src = drupal_get_path('module', 'citrus_banner') . '/images/alert_icon.svg';
    $banner_content['icons']['alert'] = $alert_src;

    $link_src = drupal_get_path('module', 'citrus_banner') . '/images/expand_more.svg';
    $banner_content['icons']['link'] = $link_src;

  return [
    '#theme' => 'citrus_crisis_banner',
    '#content' => [
      'banner' => $banner_content,
      ],
    '#attached' => [
     'library' => [
       'citrus_banner/banner',
      ],
     ],
    ];
  }
}
