<?php

namespace Drupal\citrus_banner\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'citrus_banner_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'citrus_banner.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('citrus_banner.settings');
    $form['banner_content'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Banner content'),
    ];
    $form['banner_content']['display_banner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display banner'),
      '#default_value' => $config->get('display_banner'),
    ];
    $form['banner_content']['banner_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Crisis banner text'),
      '#description' => $this->t('Text displaeyd in the crisis banner.'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('banner_text.value'),
    ];
    $form['banner_content']['link_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Crisis URL'),
      '#description' => $this->t('Insert a full URL to Crisis page.'),
      '#default_value' => $config->get('link_url'),
      '#maxlength' => 300,
    ];
    $form['banner_content']['crisis_level'] = [
      '#type' => 'select',
      '#title' => t('Crisis level.'),
      '#default_value' => $config->get('crisis_level'),
      '#description' => t('Select crisis level.'),
      '#options'  => [
      'low' => t('Low'),
      'medium' => t('Medium'),
      'high' => t('High'),
      ],
    ];

    $form['helptext'] = [
      '#markup' => '<div class="helptext-clear-cache">' . $this->t('Please note that saving this crisis banner clears caches to immediately enable it, so it might take a while.') . '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('citrus_banner.settings')
      ->set('display_banner', $values['display_banner'])
      ->set('banner_text', $values['banner_text'])
      ->set('link_url', $values['link_url'])
      ->set('crisis_level', $values['crisis_level'])
      ->save();

    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }
}
