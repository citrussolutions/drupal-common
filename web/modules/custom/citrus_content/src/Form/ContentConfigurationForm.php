<?php

namespace Drupal\citrus_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ContentConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'citrus_content_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'citrus_content.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('citrus_content.settings');

    $form['footer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Footer content'),
    ];
    $form['footer']['footer_logo'] = array(
      '#type'              => 'managed_file',
      '#upload_location'   => 'public://footer',
      '#title'             => t('Footer logo'),
      '#default_value'     => $config->get('footer_logo'),
      '#description'       => t('Add footer logo here.'),
      '#upload_validators' => ['file_validate_extensions' => 'jpg jpeg png'],
      '#required'          => FALSE,
    );
    $form['footer']['footer_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Footer text area'),
      '#description' => $this->t('Displayed next to footer logo.'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('footer_text.value'),
    ];
    $form['footer']['some_facebook'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook link target'),
      '#description' => $this->t('Where Facebook icon should link?'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('some_facebook'),
    ];
    $form['footer']['some_ig'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram link target'),
      '#description' => $this->t('Where Instagram icon should link?'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('some_ig'),
    ];
    $form['footer']['some_linkedin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LinkedIn link target'),
      '#description' => $this->t('Where LinkedIn icon should link?'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('some_linkedin'),
    ];
    $form['footer']['some_yt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Youtube link target'),
      '#description' => $this->t('Where Youtube icon should link?'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('some_yt'),
    ];
    $form['footer']['some_twitter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter link target'),
      '#description' => $this->t('Where Twitter icon should link?'),
      '#format' => 'basic_html',
      '#default_value' => $config->get('some_twitter'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config('citrus_content.settings')
      ->set('footer_logo', $values['footer_logo'])
      ->set('footer_text', $values['footer_text'])
      ->set('some_facebook', $values['some_facebook'])
      ->set('some_ig', $values['some_ig'])
      ->set('some_linkedin', $values['some_linkedin'])
      ->set('some_yt', $values['some_yt'])
      ->set('some_twitter', $values['some_twitter'])
      ->set('langcode', $language)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
