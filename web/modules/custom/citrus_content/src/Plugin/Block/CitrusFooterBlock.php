<?php

namespace Drupal\citrus_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use \Drupal\file\Entity\File;

/**
 * Provides a footer content block.
 *
 * @Block(
 *   id = "citrus_footer",
 *   admin_label = @Translation("Citrus Footer"),
 *   category = @Translation("Citrus"),
 * )
 */
class CitrusFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $content = [];
    $content['footer_logo'] = \Drupal::config('citrus_content.settings')->get('footer_logo');

    $file_id = \Drupal::config('citrus_content.settings')->get('footer_logo');
    $file = \Drupal\file\Entity\File::load($file_id['0']);
    if ($file != NULL) {
      $file_url = $file->createFileUrl();

      $content['footer_logo'] = $file_url;
    }
    $footer_text = \Drupal::config('citrus_content.settings')->get('footer_text');
    $content['footer_text'] = check_markup($footer_text['value'], 'basic_html');
    $content['some_facebook'] = \Drupal::config('citrus_content.settings')->get('some_facebook');
    $content['some_ig'] = \Drupal::config('citrus_content.settings')->get('some_ig');
    $content['some_linkedin'] = \Drupal::config('citrus_content.settings')->get('some_linkedin');
    $content['some_yt'] = \Drupal::config('citrus_content.settings')->get('some_yt');
    $content['some_twitter'] = \Drupal::config('citrus_content.settings')->get('some_twitter');
    $render_array = [
      '#theme' => 'citrus_footer',
      '#content' => $content,
    ];

    return $render_array;
  }

}
