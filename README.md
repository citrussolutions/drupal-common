# Citrus's common starter kit for Drupal

This starter kit is meant to be used when creating a new Drupal 8 project. The kit contains an installation profile, which enables commonly used modules and sets some basic settings. Also, the kit contains some helper files which are needed for example to launch the development container and to compile the style files.

The starter kit also has a script for creating a Classy subtheme with BS elements. Tested with Lando v3.0.0-rc.18 in October 2019.

## Initial project setup with [Lando](https://docs.devwithlando.io/)

1. Move to your projects directory and clone the repository.

    ```
    $ git clone git@bitbucket.org:citrussolutions/drupal-common.git PROJECTNAME
    ```

2. Replace {{PROJECT_NAME}} with your project name in .lando.yml

3. Create development environment
    ```
    $ lando start
    ```

4. Start Drupal installation script
    ```
    $ lando install-drupal
    ```

5. Check that settings.php, settings.local.php, settings.lando.php and settings.platformsh.php are alright.

6. (optional) Create Drupal theme

    ```
    $ lando create-drupal-theme
    ```

    6.1 Run dev-install.sh. See more information about gulp-workflow at [GitHub](https://github.com/CitrusSolutions/gulp-workflow)

    ```
    $ ./dev-install.sh
    ```

7. Check .gitignore, run config-export, commit everything needed and push to a new Bitbucket repository.

## Setting up a local development environment for other developers

1. Clone the repository

2. Create development environment
    ```
    $ lando start
    ```

3. Run Composer
    ```
    $ lando composer install
    ```

4. Copy settings.local.php from another developer.

5. Get database from another developer or production/stage site. Then import it with
    ```
    $ lando db-import databasedump.sql
    ```

6. (Optional) If using a regular Citrus theme, install gulp. See more information about gulp-workflow at [GitHub](https://github.com/CitrusSolutions/gulp-workflow)

    ```
    $ ./dev-install.sh
    ```

## Latest updates
- Installed Webform, uninstalled Contact.
- Reworked to use minimal and not a custom profile for easier updating.
- Added basic paragraph styles. Just add a paragraph field to your content type with content row as allowed paragraph type and everything should work pretty well.
- Added a very basic media type for image. This probably needs a bit more work and better media browser stuffs.

## To Do:

0. Finish, test and auto-install citrus_adminbar, citrus_content and citrus_language_switcher modules. They are a bit rough at the moment.

1. Add media entity types for image and video.

2. Add and configure mailsystem and htmlmailer module (swiftmailer? mandrill?).

3. Create a method for inputting inline images with a media browser.

4. Add a basic user role which can edit stuff.
