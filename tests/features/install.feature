Feature: Install validation
  In order to validate automatic installation
  I want to be able to

  Scenario: See default front front page
    Given I am an anonymous user
    And I am on "/"
    Then I should see "No front page content has been created yet."
    And I should see the link "Log in"

  @api
  Scenario: Log in
    Given users:
      | name      | status | mail             |
      | Test user | 1      | test@example.com |
    When I am logged in as "Test user"
    Then I should see the link "Log out"