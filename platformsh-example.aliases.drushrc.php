<?php

// This file is for Drush 8 and uses Airpay as an example.

// Link this file to ~/.drush

$cache_tables = array(
  'cache',
  'cache_admin_menu',
  'cache_block',
  'cache_bootstrap',
  'cache_coffee',
  'cache_eck',
  'cache_entity_og_membership',
  'cache_entity_og_membership_type',
  'cache_field',
  'cache_filter',
  'cache_form',
  'cache_image',
  'cache_libraries',
  'cache_menu',
  'cache_metatag',
  'cache_page',
  'cache_path',
  'cache_rules',
  'cache_scald',
  'cache_search_api_solr',
  'cache_shorten',
  'cache_token',
  'cache_variable',
  'cache_views',
  'cache_views_data',
  'history',
  'sessions',
);

/* Not used while developing with Lando.
$aliases['docker'] = array(
  'uri' => 'localhost:8084',
  'remote-host' => 'docker',
  'remote-user' => 'root',
  'ssh-options' => '-p 2224 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no',
  'root' => '/wwwroot/current',
  'path-aliases' => array(
#    '%files' => '/vagrant/drupal/current/sites/default/files',
    '%dump-dir' => '/tmp',
  ),
  'command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
      'no-ordered-dump' => TRUE,
      'structure-tables' => array(
        'custom' => $cache_tables,
      ),
    ),
  ),
);
*/

$aliases['prod'] = array(
  'uri' => 'https://master-7rqtwti-lplrymz6ruebs.eu-2.platformsh.site/',
  'root' => '/app/web',
  'remote-host' => 'ssh.eu-2.platform.sh',
  'remote-user' => 'lplrymz6ruebs-master-7rqtwti--app',
  'command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
      'no-ordered-dump' => TRUE,
      'structure-tables' => array(
        'custom' => $cache_tables,
      ),
    ),
  ),
);

$aliases['develop'] = array(
  'uri' => 'https://develop-sr3snxi-lplrymz6ruebs.eu-2.platformsh.site/',
  'root' => '/app/web',
  'remote-host' => 'ssh.eu-2.platform.sh',
  'remote-user' => 'lplrymz6ruebs-develop-sr3snxi--app',
  'command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
      'no-ordered-dump' => TRUE,
      'structure-tables' => array(
        'custom' => $cache_tables,
      ),
    ),
  ),
);
